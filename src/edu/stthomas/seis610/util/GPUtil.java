package edu.stthomas.seis610.util;

import java.util.Random;

public class GPUtil {
	private static GPUtil instance;
	private Random random;
	
	protected GPUtil() {
		random = new Random();
	}
	
	public static GPUtil getInstance() {
		if (instance == null) {
			instance = new GPUtil();
		}
		return instance;
	}
	
	public int getRandomInt(int n) {
		return random.nextInt(n);
	}
}
