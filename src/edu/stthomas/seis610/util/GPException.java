package edu.stthomas.seis610.util;

public class GPException extends Exception {

	public GPException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 9165583904365019563L;

}
