package edu.stthomas.seis610.tree;

import edu.stthomas.seis610.util.GPException;

public abstract class BinaryNode implements Cloneable {
	protected Object 		data;
	protected BinaryNode 	left;
	protected BinaryNode 	right;
	protected BinaryNode 	parent;
	public enum NodeType {ROOT, LEFT, RIGHT};
	
	protected NodeType		nodeType;
	
	abstract public double evaluate(double input) throws GPException;
	
	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public BinaryNode getLeft() {
		return left;
	}
	
	public void setLeft(BinaryNode left) throws GPException {
		this.left = left;
	}
	
	public BinaryNode getRight() {
		return right;
	}
	
	public void setRight(BinaryNode right) throws GPException {
		this.right = right;
	}
	
	public boolean equals(Object o) {
		return equals((BinaryNode) o);
	}
	
	public boolean equals(BinaryNode node) {
		return this.getData().equals(node.getData());
	}
	
	public int getDepth()
	{
		return getDepth(1);
	}
	
	private int getDepth(int depth) {
		if (getParent() == null)
			return depth;
		return getParent().getDepth(depth+1);
	}
	
	public void setParent(BinaryNode node)
	{
		this.parent = node;
	}
	
	public BinaryNode getParent()
	{
		return parent;
	}
	
	public void setNodeType(NodeType nodeType)
	{
		this.nodeType = nodeType;
	}
	
	public NodeType getNodeType()
	{
		return nodeType;
	}
	
	public Object clone() {
		BinaryNode clone = null;
		
		try {
			clone = (BinaryNode) super.clone();
			if (getLeft() != null) {
				clone.setLeft((BinaryNode) getLeft().clone());
				clone.getLeft().setParent(clone);
			}
			if (getRight() != null) {
				clone.setRight((BinaryNode) getRight().clone());
				clone.getRight().setParent(clone);
			}
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return clone;
	}
	
//	public BinaryNode clone(BinaryNode node) throws GPException {
//		BinaryNode clone = null;
//		clone = (BinaryNode) node.clone();
//		//clone.setData(getData().clone());
//		if (getLeft() != null)
//			clone.setLeft(clone(node.getLeft()));
//		if (getRight() != null)
//			clone.setRight(clone(node.getRight()));
//		
//		return clone;
//	}
}
