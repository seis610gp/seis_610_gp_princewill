package edu.stthomas.seis610.tree;

import java.util.LinkedList;
import java.util.List;

/**
 * A generic binary tree. No ordering is enforced, only the fact that each node
 * may only have two subnodes.
 *
 * @param <T> A sublass of BinaryNode
 */
public class BinaryTree implements Cloneable {
	protected BinaryNode root;
	
	public BinaryTree() {
		
	}
	
	public BinaryTree(BinaryNode root) {
		this.root = root;
	}

	public BinaryNode getRoot() {
		return root;
	}

	public void setRoot(BinaryNode root) {
		this.root = root;
	}
	
	public int maxDepth() {
		return (maxDepth(getRoot()));
	}

	private int maxDepth(BinaryNode node) {
		if (node == null) {
			return (0);
		} else {
			int leftDepth = maxDepth(node.getLeft());
			int rightDepth = maxDepth(node.getRight());

			// use the larger + 1
			return (Math.max(leftDepth, rightDepth) + 1);
		}
	}
	
	public List<BinaryNode> getPostOrderList() {
		return getPostOrderList(getRoot());
	}
	
//	public List<Object> getPostOrderListData() {
//		List<BinaryNode> postOrderList = getPostOrderList();
//		List<Object> valueList = new ArrayList<Object>(postOrderList.size());
//		for (BinaryNode node : postOrderList) {
//			valueList.add(node.getData());
//		}
//		return valueList;
//	}
	
	private LinkedList<BinaryNode> getPostOrderList(BinaryNode node) {
		if (node == null) {
			return null;
		} else {
			LinkedList<BinaryNode> postOrderList = new LinkedList<BinaryNode>();
			if (node.getLeft() != null) {
				postOrderList.addAll(getPostOrderList(node.getLeft()));
			}
			if (node.getRight() != null) {
				postOrderList.addAll(getPostOrderList(node.getRight()));
			}
			postOrderList.add(node);
			return postOrderList;
		}
	}
	
	public List<BinaryNode> getInOrderList() {
		return getInOrderList(getRoot());
	}
	
	private LinkedList<BinaryNode> getInOrderList(BinaryNode node) {
		if (node == null) {
			return null;
		} else {
			LinkedList<BinaryNode> inOrderList = new LinkedList<BinaryNode>();
			if (node.getLeft() != null) {
				inOrderList.addAll(getInOrderList(node.getLeft()));
			}
			inOrderList.add(node);
			if (node.getRight() != null) {
				inOrderList.addAll(getInOrderList(node.getRight()));
			}
			return inOrderList;
		}
	}
	
//	public List<OperatorNode> getOperatorNodesAtDepth(int iDepth) 
//	{
//		List<OperatorNode> operatorList= new LinkedList<OperatorNode>();
//		
//		List<BinaryNode> postOrder = getPostOrderList();
//		
//		ListIterator<BinaryNode> iterator = postOrder.listIterator();
//		
//		while (iterator.hasNext())
//		{
//			BinaryNode node = iterator.next();
//			
//			if ((node instanceof OperatorNode) && iDepth == node.getDepth())
//				operatorList.add((OperatorNode)node);
//		}
//		
//		return operatorList;
//	}
	
	public Object clone() {
		BinaryTree clone = null;
		try {
			clone = (BinaryTree) super.clone();
			clone.setRoot((BinaryNode)getRoot().clone());
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return clone;
	}

}
