package edu.stthomas.seis610.tree;

import edu.stthomas.seis610.util.GPException;

/**
 * A tree node representing one operand (for example, the integers 1-9 and x)
 *
 */
public class OperandNode extends BinaryNode {
	private boolean variable;
	
	public OperandNode(char operand) {
		setOperand(operand);
//		validate();
	}
	
	public OperandNode(double operand) {
		setData(operand);
//		validate();
	}
	
	public double evaluate(double input) {
		if (isVariable()) {
			return input;
		} else {
			return (Double) getData();
		}
	}
	
	public void setOperand(char operand) {
		// TODO throw exception if invalid operand is passed in
		if (operand == 'x') {
			setVariable(true);
		} else {
			setData(new Double(Character.digit(operand, 10)));
			setVariable(false);
		}
//		validate();
	}
	
	public void setLeft(BinaryNode left) throws GPException {
		throw new GPException("Operands cannot have subnodes.");
	}
	
	public void setRight(BinaryNode right) throws GPException {
		throw new GPException("Operands cannot have subnodes.");
	}

	public boolean isVariable() {
		return variable;
	}

	public void setVariable(boolean variable) {
		this.variable = variable;
//		validate();
	}
	
//	private void validate() {
//		if (!isVariable() && getData() == null) {
//			throw new RuntimeException("grrr");
//		}
//	}
	
	public void setData(Object data) {
		super.setData(data);
//		validate();
	}
	
	public String toString() {
		if (isVariable()) {
			return "x";
		} else {
			Double value = (Double) getData();
			int operandInt = value.intValue();
			return String.valueOf(operandInt);
		}
	}
}
