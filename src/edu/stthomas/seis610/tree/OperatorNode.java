package edu.stthomas.seis610.tree;

import edu.stthomas.seis610.util.GPException;

/**
 * A tree node representing one operator (+,-,*,/)
 *
 */
public class OperatorNode extends BinaryNode {
	
	public double evaluate(double input) throws GPException {
		double output;
		switch (getOperator()) {
			case '+':
				output = getLeft().evaluate(input) + getRight().evaluate(input);
				break;
			case '-':
				output = getLeft().evaluate(input) - getRight().evaluate(input);
				break;
			case '*':
				output = getLeft().evaluate(input) * getRight().evaluate(input);
				break;
			case '/':
				output = getLeft().evaluate(input) / getRight().evaluate(input);
				break;
			default:
				throw new GPException("Invalid operator encountered during evaluation: " + getOperator());
		}
		return output;
	}
	
	public OperatorNode(char operator) {
		setOperator(operator);
	}

	public char getOperator() {
		return (Character) getData();
	}

	public void setOperator(char operator) {
		// TODO throw exception if an invalid operator is passed in
		setData(operator);
	}
	
	public String toString() {
		return getData().toString();
	}
}
