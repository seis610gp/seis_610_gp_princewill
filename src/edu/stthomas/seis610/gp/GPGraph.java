package edu.stthomas.seis610.gp;

import java.util.*;


class GPGraph extends GPGraphic
{
	private double dMinX;
	private double dMaxX;
	private double dMinY;
	private double dMaxY;
	
	private double dXScale;
	private double dYScale;
	
	private double dOriginX;	
	private double dOriginY;

	
	private GPLine 	cAxisX;
	private GPLine	cAxisY;
	private GPPolygon graphLine, graphLine2;
	private GPString cMinX;
	private GPString cMaxX;
	private GPString cMinY;
	private GPString cMaxY;
	private GPString cName;
	private GPGraphicsView  graphView;
		
	public GPGraph(GPGraphicsView view, double dMinX, double dMaxX, double dMinY, double dMaxY, String graphName)
	{
		graphView = view;
		
		this.dMinX = dMinX;
		this.dMaxX = dMaxX;
		this.dMinY = dMinY;
		this.dMaxY = dMaxY;
		
		dXScale = view.width() / (dMaxX - dMinX);
		dYScale = view.height() / (dMaxY - dMinY);
		
		dOriginY = -dMinY * dYScale;
		dOriginX = -dMinX * dXScale;
		
		cAxisX		= new GPLine(0, dOriginY,view.width(), dOriginY);
		cAxisX.SetLineWidth(2);
		cAxisX.SetColor(255,0,0);
		cAxisY		= new GPLine(dOriginX,0,dOriginX,view.height());
		cAxisY.SetLineWidth(2);
		cAxisY.SetColor(255,0,0);
		
		Vector<GPPoint> gpVector = new Vector<GPPoint>();
		graphLine =  new GPPolygon(gpVector);
		
		graphLine2 = new GPPolygon(gpVector);
		
		cMinX = new GPString("" + (int)this.dMinX,0.0,graphView.height() / 2.0);
		cMaxX = new GPString("" + (int)this.dMaxX,graphView.width() - 30.0,graphView.height() / 2.0);
		cMinY = new GPString("" + (int)this.dMinY,graphView.width() / 2.0,5.0);
		cMaxY = new GPString("" + (int)this.dMaxY,graphView.width() / 2.0, graphView.height() - 15.0);
		cName = new GPString(graphName, 10.0,graphView.height()  - 15.0);
		
		cMinX.SetVisible(false);
		cMaxX.SetVisible(false);
		cMinY.SetVisible(false);
		cMaxY.SetVisible(false);
	}
	
	public void setGraphAxisLabels(double dMinX, boolean bMinXVisible, double dMaxX, boolean dMaxXVisible, 
									double dMinY, boolean bMinYVisible, double dMaxY, boolean dMaxYVisible)
	{
		
		cMinX.SetText(new String("" + (int)dMinX));
		cMinX.SetVisible(bMinXVisible);
		cMaxX.SetText(new String("" + (int)dMaxX));
		cMaxX.SetVisible(dMaxXVisible);
		cMinY.SetText(new String("" + (int)dMinY));
		cMinY.SetVisible(bMinYVisible);
		cMaxY.SetText(new String("" + (int)dMaxY));	
		cMaxY.SetVisible(dMaxYVisible);
		
	}
	
	public void setGraphAxis(double dMinX, double dMaxX, double dMinY, double dMaxY)
	{
		this.dMinX = dMinX;
		this.dMaxX = dMaxX;
		this.dMinY = dMinY;
		this.dMaxY = dMaxY;
		
		dXScale = graphView.width() / (dMaxX - dMinX);
		dYScale = graphView.height() / (dMaxY - dMinY);
		
		dOriginY = -dMinY * dYScale;
		dOriginX = -dMinX * dXScale;
		
		cAxisX		= new GPLine(0, dOriginY,graphView.width(), dOriginY);
		cAxisX.SetLineWidth(2);
		cAxisX.SetColor(255,0,0);
		cAxisY		= new GPLine(dOriginX,0,dOriginX,graphView.height());
		cAxisY.SetLineWidth(2);
		cAxisY.SetColor(255,0,0);	
	}
	
	public	void setGraphLine(Vector<TrainingDatum> data)
	{
		Vector<GPPoint> gpVector = new Vector<GPPoint>();
		
		Iterator<TrainingDatum> iter = data.iterator();
		while(iter.hasNext())
		{
			TrainingDatum datum= iter.next();
			double x = datum.getInput() * dXScale + dOriginX;
			double y = datum.getOutput() * dYScale + dOriginY;
			
			GPPoint point = new GPPoint(x,y);
			gpVector.add(point);
		}
		
		graphLine =  new GPPolygon(gpVector);
	}
	
	public	void setGraphLine2(Vector<TrainingDatum> data)
	{
		Vector<GPPoint> gpVector = new Vector<GPPoint>();
		
		Iterator<TrainingDatum> iter = data.iterator();
		while(iter.hasNext())
		{
			TrainingDatum datum= iter.next();
			double x = datum.getInput() * dXScale + dOriginX;
			double y = datum.getOutput() * dYScale + dOriginY;
			
			GPPoint point = new GPPoint(x,y);
			gpVector.add(point);
		}
		
		graphLine2 =  new GPPolygon(gpVector);
	}
	
	public void setGraphColor(int red, int green, int blue)
	{
		graphLine.SetColor(red,green,blue);
	}
	
	public void setGraphWidth(int width)
	{
		
		graphLine.SetPolygonWidth(width);
	}
	
	public void setGraphColor2(int red, int green, int blue)
	{
		graphLine2.SetColor(red,green,blue);
	}
	
	public void setGraphWidth2(int width)
	{
		
		graphLine2.SetPolygonWidth(width);
	}	
	
	public void setMinXPosition(double x, double y)
	{
		cMinX.SetPosition(x, y);
	}
	
	public void setMinYPosition(double x, double y)
	{
		cMinY.SetPosition(x, y);
	}
	
	public void setMaxXPosition(double x, double y)
	{
		cMaxX.SetPosition(x, y);
	}
	
	public void setMaxYPosition(double x, double y)
	{
		cMaxY.SetPosition(x, y);
	}
	
	public void draw()
	{
		cAxisX.Add();
		cAxisY.Add();
		graphLine2.Add();
		graphLine.Add();
		cMinX.Add();
		cMaxX.Add();
		cMinY.Add();
		cMaxY.Add();
		cName.Add();

	}
}