package edu.stthomas.seis610.gp;

import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stthomas.seis610.util.GPException;


public class GPDriver
{
	private GPGeneration generation;
	private static Logger logger = LoggerFactory.getLogger(GPGeneration.class);
	static GPUserInterface gui;
	static long 	startTime = 0;
	private static GPUserIntrfaceIO gpGuiIo;
	
	public void run() {
			generation = new GPGeneration();
		try {
			
			double dMinX = gpGuiIo.getMinX();
			double dMaxX = gpGuiIo.getMaxX();
			
			double dStep = ((dMaxX - dMinX) / 100.0);
			gpGuiIo.graphLine2.clear();
			for (double d1 = -10.0; d1 < 10.0; d1+=0.1)
			{
				TrainingDatum datum = new TrainingDatum(d1,(java.lang.Math.pow(d1,2) + 2)/2 );
				gpGuiIo.graphLine2.add(datum);
			}
			gpGuiIo.graphLineFitness.clear();
			
			logger.info("GP program begins.");
			generation.init();
//			System.out.println(generation.toString());
			int generationCount = 1;
			int maxGenerations = GPProperties.getMaxGenerations();
			GPIndividual individual = generation.getBestIndividual();
			double fitness = individual.getFitness();
			logger.info("Generation {}: Best fitness: {} Function: {}", new Object[] {generationCount, fitness, individual});
			while (fitness > GPProperties.getFitnessGoal() && generationCount < maxGenerations) {
				if (!gpGuiIo.isStart())
					return;
				
				generation = generation.nextGeneration();
//				System.out.println(generation.toString());
				generationCount++;
				individual = generation.getBestIndividual();
				fitness = individual.getFitness();
				logger.info("Generation {}: Best fitness: {} Function: {}", new Object[] {generationCount, fitness, individual});
				gpGuiIo.setBestFitFunctionField(individual.toString());
				gpGuiIo.setBestFitnessField(fitness);
				gpGuiIo.setGenerationNumber(generationCount);
				
				gpGuiIo.graphLine.clear();
				
				dMinX = gpGuiIo.getMinX();
				dMaxX = gpGuiIo.getMaxX();
				
				dStep = ((dMaxX - dMinX) / 100.0);
				
				for (double d = dMinX; d < dMaxX; d+=dStep)
				{
					TrainingDatum datum = new TrainingDatum(d,individual.evaluateRecursive(d));
					gpGuiIo.graphLine.add(datum);
				}
				
				long timeElapsed = System.currentTimeMillis() - startTime;
				gpGuiIo.setTimer((int)(timeElapsed / (60 * 1000F)),((int)(timeElapsed / 1000F) % 60));
				gpGuiIo.graphLineFitness.add(new TrainingDatum(((int)(timeElapsed / 1000F)),fitness));
				gpGuiIo.setTimeSeconds((long)(timeElapsed / 1000F));
				gui.procIO();
			}
			gpGuiIo.setStopRequest(true);
			logger.info("End of GP program.");
		} catch (GPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String args[])
	{
		
		
		gui = new GPUserInterface();
		gui.createBufferStrategy(2);
		gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		gpGuiIo = gui.getGuiIo();
		
		int i = 0;
		while (true)
		{
			if (gpGuiIo.isStart())
			{
				startTime = System.currentTimeMillis();
				GPDriver driver = new GPDriver();
				driver.run();
				gui.procIO();
				i = i << 1;
			} else {
				 try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}