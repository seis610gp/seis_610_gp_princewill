package edu.stthomas.seis610.gp;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.util.*;
import java.awt.image.BufferStrategy;

class GPUserIntrfaceIO
{
	private boolean bStart = false;
	private boolean bStopRequest = false;
	private String cBestFitFunctionTextField;
	private String cFitnessTextField;
	private String cGenerationNumber;
	private String cTimer;
	public Vector<TrainingDatum> graphLine;
	public Vector<TrainingDatum> graphLine2;
	public Vector<TrainingDatum> graphLineFitness;
	private long 	longTimeSeconds;
	private double 	dBestFitness;
	private double 	dMinX;
	private double  dMaxX;
	private double  dMinY;
	private double  dMaxY;
	
	public boolean isStart(){return bStart;}	
//	public boolean isStop()	{return !bStart;}
	public void setStart(boolean on){bStart = on;}
//	public void setStop(boolean on){bStart = !on;}
	public void setStopRequest(boolean on){	bStopRequest = on;}
	public boolean isStopRequest() {return bStopRequest;}
	public void setTimeSeconds(long seconds) {longTimeSeconds = seconds;}
	public long getTimeSeconds() {return longTimeSeconds;}
	public void setMinX(double size) {dMinX = size;}
	public void setMinY(double size) {dMinY = size;}
	public void setMaxX(double size) {dMaxX = size;}
	public void setMaxY(double size) {dMaxX = size;}
	public double getMinX() { return dMinX;}
	public double getMinY() { return dMinY;}
	public double getMaxX() { return dMaxX;}
	public double getMaxY() { return dMaxY;}
	
	public GPUserIntrfaceIO	()
	{
		longTimeSeconds=0;
		setBestFitFunctionField("");
		setBestFitnessField(0.0);
		setGenerationNumber(1);
		setTimer(0,0);
		graphLine = new Vector<TrainingDatum>();
		graphLine2 = new Vector<TrainingDatum>();
		graphLineFitness = new Vector<TrainingDatum>();
		
		dMinX = -10.0;
		dMaxX = 10.0;
		dMinY = -10.0;
		dMaxY = 10.0;	
	}
	

	public void setBestFitFunctionField(String func)
	{
		cBestFitFunctionTextField = "Best Fit Function: " + func;
	}
	
	public String getBestFitFunctionField() {return cBestFitFunctionTextField;}
	
	public void setBestFitnessField(double dFitness)
	{
		this.dBestFitness = dFitness;
		cFitnessTextField = "Fitness: " + dFitness;
	}
	
	public String getBestFitnessField() {return cFitnessTextField;}
	
	public double getBestFitness() {return dBestFitness;}
	
	public void setGenerationNumber(int num)
	{
		cGenerationNumber = "Generation: " + num;
	}
	
	public String getGenerationNumber() {return cGenerationNumber;}
	
	public void setTimer(int iMinutes, int iSeconds)
	{
		cTimer = "Time Elapsed: " + iMinutes + ":" + iSeconds;
	}	
	
	public String getTimer() {return cTimer;}
	
	
	
	
}

public class GPUserInterface extends JFrame implements ActionListener, AdjustmentListener
{
	private static final long serialVersionUID = 1L;
	
	private GPGraph	cBestFitFunction;
	private GPGraph cBestFitness;
	
	private Container 	container;
	private JPanel 		fieldsPanel;
	private JPanel 		scrollPanel;
	private JPanel		propertyPanel;
	
	//SOUTH
	private JButton		cStartButton, cStopButton;
	private JTextField  cOperatorsTextField;
	private JTextField 	cOperandsTextField;
	private JLabel 		cBestFitFunctionTextField;
	private JLabel 		cFitnessTextField;
	private JTextField 	cTrainingXTextField;
	private JTextField 	cTrainingYTextField;
	private JLabel 		cGenerationNumber;
	private JLabel 		cTimer;
	
	//NORTH
	private JLabel 		cFuncXMin;
	private JLabel 		cFuncXMax;
	private JLabel		cFuncYMin;
	private JLabel 		cFuncYMax;
	private JLabel		cFitXMax;
	private JLabel		cFitYMax;
	private Scrollbar 	scrollFuncXMin;
	private Scrollbar 	scrollFuncXMax;
	private Scrollbar	scrollFuncYMin;
	private Scrollbar	scrollFuncYMax;
	private Scrollbar 	scrollFitXMax;
	private Scrollbar 	scrollFitYMax;
	
	//EAST
	private JLabel		cPopulationSize_label;
	private JTextField 	cPopulationSize_textfield;
	private JLabel		cInitialTreeDepth_label;
	private JTextField 	cInitialTreeDepth_textfield;
	private JLabel		cMaxTreeDepth_label;
	private JTextField	cMaxTreeDepth_textfield;
	private JLabel		cMutationProbablity_label;
	private JTextField	cMutationProbablity_textfield;
	private JLabel 		cMutationDepth_label;
	private JTextField	cMutationDepth_textfield;
	private JLabel		cFitnessGoal_label;
	private JTextField	cFitnessGoal_textfield;
	private JLabel		cCrossoverProbability_label;
	private JTextField	cCrossoverProbability_textfield;
	private JLabel		cTournamentSize_label;
	private JTextField	cTournamentsize_textfield;
	private JLabel		cMaxGenerations_label;
	private JTextField	cMaxGenerations_textfield;
	
	
	
	GPGraphicsView		cBestFuncView;
	GPGraphicsView		cBestFitnessView;
	
	protected GPUserIntrfaceIO gpIO;
	
	public GPUserInterface()
	{
		super ("Genetic Programming SEIS 610");
		gpIO = new GPUserIntrfaceIO();
		
		cFuncXMin	= new JLabel("Best Function Min X: -10");
		cFuncXMax	= new JLabel("Best Function Max X: 10");
		cFuncYMin	= new JLabel("Best Function Min Y: -10");
		cFuncYMax	= new JLabel("Best Function Max Y: 10");
		cFitXMax	= new JLabel("Fitness Graph Max Time: 20");
		cFitYMax	= new JLabel("Fitness Graph Max Fit: 25");
		
		
		scrollFuncXMin	= new Scrollbar(Scrollbar.HORIZONTAL,-10,1,-100,-1);
		scrollFuncXMin.addAdjustmentListener(this);
		scrollFuncXMax	= new Scrollbar(Scrollbar.HORIZONTAL,10,1,1,100);
		scrollFuncXMax.addAdjustmentListener(this);
		scrollFuncYMin	= new Scrollbar(Scrollbar.HORIZONTAL,-10,1,-100,-1);
		scrollFuncYMin.addAdjustmentListener(this);
		scrollFuncYMax	= new Scrollbar(Scrollbar.HORIZONTAL,10,1,1,100);
		scrollFuncYMax.addAdjustmentListener(this);
		scrollFitXMax 	= new Scrollbar(Scrollbar.HORIZONTAL,1,20,1,900);
		scrollFitXMax.addAdjustmentListener(this);	
		scrollFitYMax	= new Scrollbar(Scrollbar.HORIZONTAL,25,5,0,100);
		scrollFitYMax.addAdjustmentListener(this);
		
		scrollPanel = new JPanel();
		scrollPanel.setLayout (new GridLayout(2,6));
		
		scrollPanel.add(cFuncXMin);
		scrollPanel.add(cFuncXMax);
		scrollPanel.add(cFuncYMin);
		scrollPanel.add(cFuncYMax);
		scrollPanel.add(cFitXMax);
		scrollPanel.add(cFitYMax);
		
		
		scrollPanel.add(scrollFuncXMin);
		scrollPanel.add(scrollFuncXMax);
		scrollPanel.add(scrollFuncYMin);
		scrollPanel.add(scrollFuncYMax);
		scrollPanel.add(scrollFitXMax);
		scrollPanel.add(scrollFitYMax);
		
		propertyPanel = new JPanel();
		propertyPanel.setLayout(new GridLayout(9,2));
		
		cPopulationSize_label = new JLabel("Population Size:");
		cPopulationSize_textfield = new JTextField(""+GPProperties.getPopulationSize());
		cPopulationSize_textfield.addActionListener(this);
		
		cInitialTreeDepth_label = new JLabel("Init Tree Depth:");
		cInitialTreeDepth_textfield = new JTextField("" + GPProperties.getInitialTreeDepth());
		cInitialTreeDepth_textfield.addActionListener(this);
		
		cMaxTreeDepth_label = new JLabel("Max Tree Depth:");
		cMaxTreeDepth_textfield = new JTextField("" + GPProperties.getMaxTreeDepth());
		cMaxTreeDepth_textfield.addActionListener(this);
		
		cMutationProbablity_label= new JLabel("Mutation Probablity:");
		cMutationProbablity_textfield = new JTextField("" + GPProperties.getMutationProbablity());
		cMutationProbablity_textfield.addActionListener(this);
		
		cMutationDepth_label = new JLabel("Mutation Depth:");
		cMutationDepth_textfield = new JTextField("" + GPProperties.getMaxMutationDepth());
		cMutationDepth_textfield.addActionListener(this);
		
		cFitnessGoal_label = new JLabel("Fitness Goal:");
		cFitnessGoal_textfield = new JTextField("" + GPProperties.getFitnessGoal());
		cFitnessGoal_textfield.addActionListener(this);
		
		cCrossoverProbability_label = new JLabel("Crossover Probability:");
		cCrossoverProbability_textfield = new JTextField("" + GPProperties.getCrossoverProbability());
		cCrossoverProbability_textfield.addActionListener(this);
		
		cTournamentSize_label = new JLabel("Tournament Size:");
		cTournamentsize_textfield = new JTextField("" + GPProperties.getTournamentSize());
		cTournamentsize_textfield.addActionListener(this);
		
		cMaxGenerations_label = new JLabel("Max Generations:");
		cMaxGenerations_textfield = new JTextField("" + GPProperties.getMaxGenerations());
		cMaxGenerations_textfield.addActionListener(this);	
	
		propertyPanel.add(cPopulationSize_label);
		propertyPanel.add(cPopulationSize_textfield);
		propertyPanel.add(cInitialTreeDepth_label);
		propertyPanel.add(cInitialTreeDepth_textfield);
		propertyPanel.add(cMaxTreeDepth_label);
		propertyPanel.add(cMaxTreeDepth_textfield);
		propertyPanel.add(cMutationProbablity_label);
		propertyPanel.add(cMutationProbablity_textfield);
		propertyPanel.add(cMutationDepth_label);
		propertyPanel.add(cMutationDepth_textfield);
		propertyPanel.add(cFitnessGoal_label);
		propertyPanel.add(cFitnessGoal_textfield);
		propertyPanel.add(cCrossoverProbability_label);
		propertyPanel.add(cCrossoverProbability_textfield);
		propertyPanel.add(cTournamentSize_label);
		propertyPanel.add(cTournamentsize_textfield);
		propertyPanel.add(cMaxGenerations_label);
		propertyPanel.add(cMaxGenerations_textfield);
		
		
		cStartButton 	= new JButton("Start");
		cStartButton.setActionCommand("start");
		cStartButton.setEnabled(true);
		cStartButton.addActionListener(this);
		cStopButton 	= new JButton("Stop");
		cStopButton.setActionCommand("stop");
		cStopButton.setEnabled(false);
		cStopButton.addActionListener(this);
		cOperatorsTextField = new JTextField("Operators: " + GPProperties.getOperatorString());
		cOperatorsTextField.setEditable(false);
		cOperandsTextField = new JTextField("Operands: " + GPProperties.getOperandString());
		cOperandsTextField.setEditable(false);
		cBestFitFunctionTextField = new JLabel("Best Fit Function:");
		cFitnessTextField = new JLabel("Fitness: ");	
		cTrainingXTextField = new JTextField("Training X: " + GPProperties.getTrainigXString());
		cTrainingXTextField.setEditable(false);
		cTrainingYTextField = new JTextField("Training Y: " + GPProperties.getTrainingYString());
		cTrainingYTextField.setEditable(false);
		cGenerationNumber = new JLabel("Generation: 0");
		cTimer = new JLabel("Time Elapsed: 0:0");
		
		fieldsPanel = new JPanel();
		fieldsPanel.setLayout (new GridLayout(5,2));

		fieldsPanel.add(cStartButton);
		fieldsPanel.add(cStopButton);
		fieldsPanel.add(cBestFitFunctionTextField);
		fieldsPanel.add(cFitnessTextField);
		fieldsPanel.add(cGenerationNumber);
		fieldsPanel.add(cTimer);
		fieldsPanel.add(cTrainingXTextField);
		fieldsPanel.add(cTrainingYTextField);
		fieldsPanel.add(cOperatorsTextField);
		fieldsPanel.add(cOperandsTextField);
	
			
		container	= getContentPane();
		
		container.add(scrollPanel,BorderLayout.NORTH);
		container.add(propertyPanel,BorderLayout.EAST);
		container.add(fieldsPanel,BorderLayout.SOUTH);
		
		cBestFuncView 		= new GPGraphicsView(10,100,400,400,new String("func"));
		cBestFitnessView	= new GPGraphicsView(500,110,400,400,new String("fitness"));
		
		//for now
		cBestFitFunction 	=	new GPGraph(cBestFuncView, -10, 10,-10, 10, "Best Fit Individual Graph");
		cBestFitFunction.setGraphAxisLabels(-10, true, 10, true, -10, true, 10, true);
	
		cBestFitness		= 	new GPGraph(cBestFitnessView,0,scrollFitXMax.getValue(),0,scrollFitYMax.getValue(), "Fitness Graph");
		cBestFitness.setMaxXPosition(370, 20);
		cBestFitness.setMaxYPosition(20, 370);
		cBestFitness.setGraphAxisLabels(0, false, scrollFitXMax.getValue(), true, 0, false, scrollFitYMax.getValue(), true);
	

		setSize(1280,768);
		setVisible(true);
		
		  while(getBufferStrategy() == null)
              this.createBufferStrategy(2);
	}
	
	

	public void paint(Graphics g)
	{
		BufferStrategy bf = this.getBufferStrategy();
		
		if (bf == null)
			return;
		g = null; 
		try
		{
		g = bf.getDrawGraphics();
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		cBestFuncView.SetBackgroundColor(211,211,211);
		cBestFuncView.SetGraphicsContent(g2d);
		GPGraphic.SetCurrentView(cBestFuncView);
		cBestFitFunction.setGraphColor(0, 255, 0);
		cBestFitFunction.setGraphWidth(3);
		cBestFitFunction.setGraphColor2(0, 0, 255);
		cBestFitFunction.setGraphWidth2(6);	
		cBestFitFunction.draw();
		GPGraphic.DrawViewElements();
		
		cBestFitnessView.SetBackgroundColor(210,180,140);
		cBestFitnessView.SetGraphicsContent(g2d);	
		GPGraphic.SetCurrentView(cBestFitnessView);
		cBestFitness.setGraphColor(0, 191, 255);
		cBestFitness.setGraphWidth(3);
		cBestFitness.draw();
		GPGraphic.DrawViewElements();
		}
		finally
		{
			g.dispose();
		}
		bf.show();
		 
  	}
	
	public void actionPerformed(ActionEvent e)
	{
		Object eSource = e.getSource();
		
		if (eSource == cPopulationSize_textfield)
		{
			try{
				
			Integer size =	Integer.parseInt(cPopulationSize_textfield.getText());
			
			if (size <= 0 || size > 50000)
				throw new NumberFormatException();
		
			GPProperties.setPopulationSize(size);
			}
			catch (NumberFormatException nfe) {
				cPopulationSize_textfield.setText("" + GPProperties.getPopulationSize());
			}
		}
		else if (eSource == cInitialTreeDepth_textfield)
		{
			try{
				Integer size = Integer.parseInt(cInitialTreeDepth_textfield.getText());
				
				if (size < 2 || size > 6)
					throw new NumberFormatException();
				
				GPProperties.setInitialTreeDepth(size);
				}
				catch (NumberFormatException nfe) {
					cInitialTreeDepth_textfield.setText("" + GPProperties.getInitialTreeDepth());
				}
		
		}	
		else if (eSource ==cMaxTreeDepth_textfield)
		{
				try{
				Integer size = Integer.parseInt(cMaxTreeDepth_textfield.getText());
				
				if (size < 2 || size > 10)
					throw new NumberFormatException();
				
				GPProperties.setMaxTreeDepth(size);
				}
				catch (NumberFormatException nfe) {
					cMaxTreeDepth_textfield.setText("" + GPProperties.getMaxTreeDepth());
				}	
		}
		else if (eSource == cMutationProbablity_textfield)
		{
			try{
				
				Double size = Double.parseDouble(cMutationProbablity_textfield.getText());
				
				if (size <= 0.0 || size > 0.9)
					throw new NumberFormatException();
				
				GPProperties.setMutationProbablity(size);
				}
				catch (NumberFormatException nfe) {
					cMutationProbablity_textfield.setText("" + GPProperties.getMutationProbablity());
				}		
		}
		else if (eSource == cMutationDepth_textfield)
		{
				try{
				
				Integer size = Integer.parseInt(cMutationDepth_textfield.getText());
				
				if (size <=2 || size > 10)
					throw new NumberFormatException();
					
				GPProperties.setMaxMutationDepth(size);
				}
				catch (NumberFormatException nfe) {
					cMutationDepth_textfield.setText("" + GPProperties.getMaxMutationDepth());
				}				
		}
		else if (eSource == cFitnessGoal_textfield)
		{
				try{
					
				Double size = 	Double.parseDouble(cFitnessGoal_textfield.getText());
				
				if (size < 0.0)
					throw new NumberFormatException();
				
				GPProperties.setFitnessGoal(size);
				}
				catch (NumberFormatException nfe) {
					cFitnessGoal_textfield.setText("" + GPProperties.getFitnessGoal());
				}	
		}
		else if (eSource == cCrossoverProbability_textfield)
		{
				try{
					
				Double size = 	Double.parseDouble(cCrossoverProbability_textfield.getText());
				
				if (size < 0.0 || size > 1.0)
					throw new NumberFormatException();
				
				GPProperties.setCrossoverProbability(size);
				}
				catch (NumberFormatException nfe) {
					cCrossoverProbability_textfield.setText("" + GPProperties.getCrossoverProbability());
				}	
		}
		else if (eSource == cTournamentsize_textfield)
		{
				try{
					
				Integer size = 	Integer.parseInt(cTournamentsize_textfield.getText());
				
				if (size <= 0 || size > 10)
					throw new NumberFormatException();
				
				GPProperties.setTournamentSize(size);
				}
				catch (NumberFormatException nfe) {
					cTournamentsize_textfield.setText("" + GPProperties.getTournamentSize());
				}		
		}
		else if (eSource == cMaxGenerations_textfield)
		{
				try{
					
					Integer size = Integer.parseInt(cMaxGenerations_textfield.getText());
					
					if (size <= 0 || size > 100000)
						throw new NumberFormatException();
						
				
				GPProperties.setMaxGenerations(size);
				}
				catch (NumberFormatException nfe) {
					cMaxGenerations_textfield.setText("" + GPProperties.getMaxGenerations());
				}	
		}
		else if ("start".equals(e.getActionCommand()))
		{
			cStartButton.setEnabled(false);
			cStopButton.setEnabled(true);
			gpIO.setStart(true);
//			gpIO.setStop(false);
			scrollFitXMax.setValue(5);
			scrollFitYMax.setValue(25);
			cBestFitness.setGraphAxis(0.0, (double)scrollFitXMax.getValue(), 0.0, (double)scrollFitYMax.getValue());
			cBestFitness.setGraphAxisLabels(0.0,false,(double)scrollFitXMax.getValue(),true,0.0,false, (double)scrollFitYMax.getValue(), true);
			SetFieldsEditable(false);	
		}
		else if ("stop".equals(e.getActionCommand()))
		{
			cStartButton.setEnabled(true);
			cStopButton.setEnabled(false);
			gpIO.setStart(false);
//			gpIO.setStop(true);
			SetFieldsEditable(true);
		}
		
	}
	
	public void adjustmentValueChanged(AdjustmentEvent e)
	{
		Scrollbar scroll = (Scrollbar)e.getSource();
		
		if (scroll == scrollFuncXMin || scroll == scrollFuncXMax || scroll == scrollFuncYMin || scroll == scrollFuncYMax)
		{
			cBestFitFunction.setGraphAxis((double)scrollFuncXMin.getValue(), (double)scrollFuncXMax.getValue(), (double)scrollFuncYMin.getValue(), (double)scrollFuncYMax.getValue());
			cBestFitFunction.setGraphLine(gpIO.graphLine);
			cBestFitFunction.setGraphLine2(gpIO.graphLine2);
			cBestFitFunction.setGraphAxisLabels((double)scrollFuncXMin.getValue(), true, (double)scrollFuncXMax.getValue(), true, (double)scrollFuncYMin.getValue(), true, (double)scrollFuncYMax.getValue(), true);
			
			cFuncXMin.setText("Best Function Min X: " + scrollFuncXMin.getValue());
			cFuncXMax.setText("Best Function Max X: " + scrollFuncXMax.getValue());
			cFuncYMin.setText("Best Function Min Y: " + scrollFuncYMin.getValue());
			cFuncYMax.setText("Best Function Max Y: " + scrollFuncYMax.getValue());
		
			repaint((int)cBestFuncView.dLeftX, (int)cBestFuncView.dTopY, (int)cBestFuncView.width(), (int)cBestFuncView.height());
		}
		else if (scroll == scrollFitXMax || scroll == scrollFitYMax)
		{
			cBestFitness.setGraphAxis(0.0, (double)scrollFitXMax.getValue(), 0.0, (double)scrollFitYMax.getValue());
			cBestFitness.setGraphLine(gpIO.graphLineFitness);
			cBestFitness.setGraphAxisLabels(0.0,false,(double)scrollFitXMax.getValue(),true,0.0,false, (double)scrollFitYMax.getValue(), true);
			
			cFitXMax.setText("Fitness Graph Max Time: " + scrollFitXMax.getValue());
			cFitYMax.setText("Fitness Graph Max Fit: " + scrollFitYMax.getValue());

			
			repaint((int)cBestFitnessView.dLeftX,(int)cBestFuncView.dTopY, (int)cBestFuncView.width(),(int)cBestFuncView.height());
		}
		
	}
	
	private void SetFieldsEditable(boolean edit)
	{
		//EAST
		cPopulationSize_textfield.setEditable(edit);
		cInitialTreeDepth_textfield.setEditable(edit);
		cMaxTreeDepth_textfield.setEditable(edit);
		cMutationProbablity_textfield.setEditable(edit);
		cMutationDepth_textfield.setEditable(edit);
		cFitnessGoal_textfield.setEditable(edit);
		cCrossoverProbability_textfield.setEditable(edit);
		cTournamentsize_textfield.setEditable(edit);
		cMaxGenerations_textfield.setEditable(edit);	
	}
	
	
	
	
	public GPUserIntrfaceIO getGuiIo()
	{
		return gpIO;
		
	}
	
	public void procIO()
	{
		if (gpIO.isStopRequest())
		{
			cStartButton.setEnabled(true);
			cStopButton.setEnabled(false);
			gpIO.setStart(false);
//			gpIO.setStop(true);	
			gpIO.setStopRequest(false);
			SetFieldsEditable(true);
		}	
		
		if (gpIO.getTimeSeconds() > scrollFitXMax.getValue())
		{
			if (gpIO.getTimeSeconds() > scrollFitXMax.getMaximum())
				scrollFitXMax.setMaximum((int)gpIO.getTimeSeconds());
			
			scrollFitXMax.setValue((int)gpIO.getTimeSeconds());
			cBestFitness.setGraphAxis(0.0, (double)scrollFitXMax.getValue(), 0.0, (double)scrollFitYMax.getValue());
			cBestFitness.setGraphLine(gpIO.graphLineFitness);
			cBestFitness.setGraphAxisLabels(0.0,false,(double)scrollFitXMax.getValue(),true,0.0,false, (double)scrollFitYMax.getValue(), true);
			
			cFitXMax.setText("Fitness Graph Max Time: " + scrollFitXMax.getValue());
			cFitYMax.setText("Fitness Graph Max Fit: " + scrollFitYMax.getValue());	
		}
		
		if (gpIO.getBestFitness() > scrollFitYMax.getValue())
		{
			if (gpIO.getBestFitness() > scrollFitYMax.getMaximum())
				scrollFitYMax.setMaximum((int)gpIO.getBestFitness());
			
			scrollFitYMax.setValue((int)gpIO.getBestFitness());
			
			cBestFitness.setGraphAxis(0.0, (double)scrollFitXMax.getValue(), 0.0, (double)scrollFitYMax.getValue());
			cBestFitness.setGraphLine(gpIO.graphLineFitness);
			cBestFitness.setGraphAxisLabels(0.0,false,(double)scrollFitXMax.getValue(),true,0.0,false, (double)scrollFitYMax.getValue(), true);
			
			cFitXMax.setText("Fitness Graph Max Time: " + scrollFitXMax.getValue());
			cFitYMax.setText("Fitness Graph Max Fit: " + scrollFitYMax.getValue());	
			
		}
			
		gpIO.setMinX(scrollFuncXMin.getValue());
		gpIO.setMinY(scrollFuncYMin.getValue());
		gpIO.setMaxX(scrollFuncXMax.getValue());
		gpIO.setMaxY(scrollFuncYMax.getValue());
		
				
		cFitnessTextField.setText(gpIO.getBestFitnessField());
		cBestFitFunctionTextField.setText(gpIO.getBestFitFunctionField());
		cGenerationNumber.setText(gpIO.getGenerationNumber());
		cTimer.setText(gpIO.getTimer());
		cBestFitFunction.setGraphLine(gpIO.graphLine);
		cBestFitFunction.setGraphLine2(gpIO.graphLine2);
		cBestFitness.setGraphLine(gpIO.graphLineFitness);
		
		
		
		
		repaint((int)cBestFuncView.dLeftX, (int)cBestFuncView.dTopY,(int)cBestFuncView.width(),(int)cBestFuncView.height());
	}

	
}