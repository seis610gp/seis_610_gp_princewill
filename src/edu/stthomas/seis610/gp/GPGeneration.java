package edu.stthomas.seis610.gp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stthomas.seis610.tree.BinaryNode;
import edu.stthomas.seis610.tree.BinaryNode.NodeType;
import edu.stthomas.seis610.util.GPException;
import edu.stthomas.seis610.util.GPUtil;

public class GPGeneration {
	private List<GPIndividual> population;
	
	private static Logger logger = LoggerFactory.getLogger(GPGeneration.class);
	
	public GPGeneration() {
		population = new ArrayList<GPIndividual>(GPProperties.getPopulationSize());
	}
	
	/**
	 * Initialize the population using the ramped-half-and-half technique.
	 * 
	 * @throws GPException
	 */
	public void init() throws GPException {
		int maxDepth = GPProperties.getInitialTreeDepth();
		int batchCount = GPProperties.getPopulationSize() / ((maxDepth-1)*2);
		
		for (int depth=2; depth<=maxDepth; depth++) {
			for (int i=0; i<batchCount; i++) {
				population.add(GPFactory.fullIndividual(depth));
				population.add(GPFactory.growIndividual(depth));
			}
		}
		logger.info("Population initialized. Size: {}", population.size());
	}
	
	public GPGeneration nextGeneration() throws GPException {
		GPGeneration nextGeneration = new GPGeneration();
//		nextGeneration.setPopulation(naturalSelection());
		nextGeneration.getPopulation().addAll(reproduction());
		nextGeneration.getPopulation().addAll(crossover());
		nextGeneration.mutate();
		return nextGeneration;
	}
	
	public GPIndividual getBestIndividual() throws GPException {
		GPIndividual bestIndividual = population.get(0);
		Double bestFitness = bestIndividual.getFitness();
		if (bestFitness.isNaN()) // All comparisons to NaN are false, don't allow it!
			bestFitness = Double.POSITIVE_INFINITY;
		for (int i=1; i<population.size(); i++) {
			if (population.get(i).getFitness() < bestFitness) {
				bestIndividual = population.get(i);
				bestFitness = bestIndividual.getFitness();
			}
		}
		
		return bestIndividual;
	}
	
//	public GPIndividual getBestIndividual() {
//		Collections.sort(population); // TODO ensure sort is not called twice per generation
//		return getPopulation().get(0);
//	}
	
//	private List<GPIndividual> naturalSelection() {
//		// Sort the population by fitness
//		Collections.sort(population);
//		// Remove those with the least fitness
//		int removeCount = population.size() / 2;
//		return population.subList(0, removeCount);
//	}
	
	private List<GPIndividual> reproduction() throws GPException {
		double reproductionProbability = 1 - GPProperties.getCrossoverProbability();
		int reproductionCount = (int) Math.round(reproductionProbability * GPProperties.getPopulationSize());
		List<GPIndividual> reproduction = new ArrayList<GPIndividual>(reproductionCount);
		for (int i=0; i<reproductionCount; i++) {
			reproduction.add(tournamentSelection());
		}
		return reproduction;
	}
	
	private GPIndividual tournamentSelection() throws GPException {
		return tournamentSelection(0);
	}
	
	private GPIndividual tournamentSelection(int recursion) throws GPException {
		int tournamentSize = GPProperties.getTournamentSize();
		List<GPIndividual> tournament = new ArrayList<GPIndividual>(tournamentSize);
		for (int i=0; i<tournamentSize; i++) {
			int randomIndex = GPUtil.getInstance().getRandomInt(population.size());
			tournament.add(population.get(randomIndex));
		}
		// Sort the tournament by fitness
		Collections.sort(tournament);
		// Get the most fit individual
		GPIndividual winner = tournament.get(0);
		// If the winner is NaN, try again
		if (new Double(winner.getFitness()).isNaN()) {
			if (recursion >= 5) // Enough already, just generate a new individual
				winner = GPFactory.growIndividual(GPProperties.getInitialTreeDepth());
			else // run another tournament selection
				winner = tournamentSelection(++recursion);
		}

		return winner;
	}
	
	private List<GPIndividual> crossover() throws GPException {
		double crossoverProbability = GPProperties.getCrossoverProbability();
		int crossoverCount = (int) Math.round(crossoverProbability * GPProperties.getPopulationSize());
		List<GPIndividual> crossover = new ArrayList<GPIndividual>(crossoverCount);
		while(crossover.size() < crossoverCount) {
			GPIndividual parent1 = tournamentSelection();
			GPIndividual parent2 = tournamentSelection();
			crossover.addAll(crossoverOperator(parent1, parent2));
		}
		return crossover;
	}
	
	private List<GPIndividual> crossoverOperator (GPIndividual indv1, GPIndividual indv2) throws GPException {
		// Get deep copies of each individual
		GPIndividual offspring1 = (GPIndividual) indv1.clone();
		GPIndividual offspring2 = (GPIndividual) indv2.clone();
		
		// Reset fitness for offspring
		offspring1.setFitness(null);
		offspring2.setFitness(null);
		
		// Select a node from each individual		
		BinaryNode nodeInd1 = offspring1.getRandomNode();
		BinaryNode nodeInd2 = offspring2.getRandomNode();
		
		// Now swap the selected nodes by updating the appropriate references
		BinaryNode parent1 = nodeInd1.getParent();
		BinaryNode parent2 = nodeInd2.getParent();
		
		NodeType nodeInd1Type = nodeInd1.getNodeType();
		NodeType nodeInd2Type = nodeInd2.getNodeType(); 

		if (nodeInd1Type == BinaryNode.NodeType.LEFT) {
			parent1.setLeft(nodeInd2);
			nodeInd2.setNodeType(BinaryNode.NodeType.LEFT);
			nodeInd2.setParent(parent1);
		} else if (nodeInd1Type == BinaryNode.NodeType.RIGHT) {
			parent1.setRight(nodeInd2);
			nodeInd2.setNodeType(BinaryNode.NodeType.RIGHT);
			nodeInd2.setParent(parent1);
		} else {
			offspring1.setRoot(nodeInd2);
			nodeInd2.setNodeType(BinaryNode.NodeType.ROOT);
			nodeInd2.setParent(null);
		}

		if (nodeInd2Type == BinaryNode.NodeType.LEFT) {
			parent2.setLeft(nodeInd1);
			nodeInd1.setNodeType(BinaryNode.NodeType.LEFT);
		} else if (nodeInd2Type == BinaryNode.NodeType.RIGHT) {
			parent2.setRight(nodeInd1);
			nodeInd1.setNodeType(BinaryNode.NodeType.RIGHT);
		} else {
			offspring2.setRoot(nodeInd1);
			nodeInd1.setNodeType(BinaryNode.NodeType.ROOT);
			nodeInd1.setParent(null);
		}
		nodeInd1.setParent(parent2);
		
		// If max depth exceeded, return the parent instead
		if (offspring1.maxDepth() > GPProperties.getMaxTreeDepth())
			offspring1 = indv1;
		if (offspring2.maxDepth() > GPProperties.getMaxTreeDepth())
			offspring2 = indv2;
		
		// Return the offspring
		List<GPIndividual> offspring = new ArrayList<GPIndividual>(2);
		offspring.add(offspring1);
		offspring.add(offspring2);
		return offspring;
	}
	
	private void mutate() throws GPException {		
		int mutateCount = (int) (getPopulation().size() * GPProperties.getMutationProbablity());
		for (int i=0; i<mutateCount; i++) {
			int mutateIndex = GPUtil.getInstance().getRandomInt(getPopulation().size());
			getPopulation().get(mutateIndex).mutate();
		}
	}

	public List<GPIndividual> getPopulation() {
		return population;
	}

	public void setPopulation(List<GPIndividual> population) {
		this.population = population;
	}
	
	public String toString() {
		StringBuffer output = new StringBuffer();
		for (GPIndividual individual : population) {
			output.append(individual.toString() + "\n");
		}
		return output.toString();
	}
}
