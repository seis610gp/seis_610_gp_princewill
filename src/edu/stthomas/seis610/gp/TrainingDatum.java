package edu.stthomas.seis610.gp;

public class TrainingDatum {
	private double input;
	private double output;
	
	public TrainingDatum(double input, double output) {
		setInput(input);
		setOutput(output);
	}
	
	public double getInput() {
		return input;
	}
	public void setInput(double input) {
		this.input = input;
	}
	public double getOutput() {
		return output;
	}
	public void setOutput(double output) {
		this.output = output;
	}
}
