package edu.stthomas.seis610.gp;

import java.util.List;

import edu.stthomas.seis610.tree.BinaryNode;
import edu.stthomas.seis610.tree.OperandNode;
import edu.stthomas.seis610.tree.OperatorNode;
import edu.stthomas.seis610.tree.BinaryNode.NodeType;
import edu.stthomas.seis610.util.GPException;
import edu.stthomas.seis610.util.GPUtil;

public class GPFactory {
	private static List<Character> operatorList = GPProperties.getOperatorList();
	private static List<Character> operandList = GPProperties.getOperandList();
	private static List<Character> independentVarList = GPProperties.getIndependentOperandList();
	private static List<TrainingDatum> trainingData = GPProperties.getTrainingData();
	
	public static GPIndividual fullIndividual(int maxDepth) throws GPException {
		GPIndividual individual;
		
		if (maxDepth == 1) {
			OperandNode root = getRandomOperand();
			root.setNodeType(NodeType.ROOT);
			individual = new GPIndividual(root);
		} else {
			OperatorNode root = getRandomOperator();
			root.setNodeType(NodeType.ROOT);
			individual = new GPIndividual(root);
			individual.setTrainingData(trainingData);
			addChildren(root, 1, maxDepth);
		}
		
		return individual;
	}
	
	public static GPIndividual growIndividual(int maxDepth) throws GPException {
		GPIndividual individual;
		
		if (maxDepth == 1) {
			OperandNode root = getRandomOperand();
			root.setNodeType(NodeType.ROOT);
			individual = new GPIndividual(root);
		} else {
			OperatorNode root = getRandomOperator();
			root.setNodeType(NodeType.ROOT);
			individual = new GPIndividual(root);
			individual.setTrainingData(trainingData);
			growChildren(root, 1, maxDepth);
		}
		
		return individual;
	}
	
	private static void addChildren(BinaryNode node, int depth, int maxDepth) throws GPException {
		BinaryNode left;
		BinaryNode right;
		
		if (depth == maxDepth)
			return;
		else if (depth == maxDepth-1) {
			left = getRandomOperand();
			right = getRandomOperand();
		} else {
			left = getRandomOperator();
			right = getRandomOperator();
		}
		left.setNodeType(NodeType.LEFT);
		right.setNodeType(NodeType.RIGHT);
		node.setLeft(left);
		node.setRight(right);
		left.setParent(node);
		right.setParent(node);
		addChildren(left, depth+1, maxDepth);
		addChildren(right, depth+1, maxDepth);
	}
	
	private static void growChildren(BinaryNode node, int depth, int maxDepth) throws GPException {
		BinaryNode left;
		BinaryNode right;
		
		if (depth == maxDepth || node instanceof OperandNode)
			return;
		else if (depth == maxDepth-1) {
			left = getRandomOperand();
			right = getRandomOperand();
		} else {
			left = getRandomOperatorOrOperand();
			right = getRandomOperatorOrOperand();
		}
		left.setNodeType(NodeType.LEFT);
		right.setNodeType(NodeType.RIGHT);
		node.setLeft(left);
		node.setRight(right);
		left.setParent(node);
		right.setParent(node);
		growChildren(left, depth+1, maxDepth);
		growChildren(right, depth+1, maxDepth);
	}
	
	public static OperatorNode getRandomOperator() {
		int index = GPUtil.getInstance().getRandomInt(operatorList.size());
		char operator = operatorList.get(index);

		return new OperatorNode(operator);
	}

	public static OperandNode getRandomOperand() {
		int index = GPUtil.getInstance().getRandomInt(operandList.size());
		char operand = operandList.get(index);

		return new OperandNode(operand);
	}
	
	public static BinaryNode getRandomOperatorOrOperand() {
		if (GPUtil.getInstance().getRandomInt(2) == 1)
			return getRandomOperator();
		else {
			return getRandomOperand();
		}
	}
	
	public static OperandNode getRandomIndependentVariable()
	{
		int index = GPUtil.getInstance().getRandomInt(independentVarList.size());
		char operand = independentVarList.get(index);

		return new OperandNode(operand);
		
	}
}


