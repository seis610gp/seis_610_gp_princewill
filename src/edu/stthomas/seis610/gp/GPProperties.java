package edu.stthomas.seis610.gp;

import java.util.*;
import java.io.*;




public class GPProperties extends Properties
{
	public static final String propertiesFile = "init.properties";//relative path for now
	private static GPProperties instance;
	private static final long serialVersionUID = 1L;
	private static OutputStream outputStream;
	
	protected GPProperties() 
	{
		try {
			InputStream inputStream = this.getClass().getClassLoader()
			.getResourceAsStream(propertiesFile);
			load(inputStream);
		} catch (IOException e) {
			System.err.println("IOException: Unable to load properties file for GPProperties. INPUT");
		}

		
	}
	
	public static int getPopulationSize()
	{
		return getInstance().getIntProperty("populationSize");
	}
	
	public static void setPopulationSize(Integer size)
	{
		
		setIntProperty("populationSize",size);
	}
	
	public static int getInitialTreeDepth()
	{
		return getInstance().getIntProperty("initialTreeDepth");
	}
	
	public static void setInitialTreeDepth(Integer size )
	{
		setIntProperty("initialTreeDepth",size);
	
	}
	
	public static int getMaxTreeDepth()
	{
		return getInstance().getIntProperty("maxTreeDepth");
	}
	
	public static void setMaxTreeDepth(Integer size)
	{
		setIntProperty("maxTreeDepth",size);
	
		
	}
	
	
	public static double getMutationProbablity()
	{
		return getInstance().getDoubleProperty("mutationProbability");
	}
	
	public static void 	setMutationProbablity(Double size)
	{
		setDoubleProperty("mutationProbability",size);
		
	}
	
	public static int getMaxMutationDepth() {
		return getInstance().getIntProperty("maxMutationDepth");
	}
	
	public static void setMaxMutationDepth(Integer size)
	{
		
		setIntProperty("maxMutationDepth",size);
	}
	
	public static double getFitnessGoal()
	{
		return getInstance().getDoubleProperty("fitnessGoal");
	}
	
	public static void setFitnessGoal(Double size)
	{
		
		setDoubleProperty("fitnessGoal",size);
	}
	
	public static String getOperatorString()
	{
		return getInstance().getProperty("operators");
	}
	
	public static List<Character> getOperatorList()
	{
		LinkedList<Character> opLinkedList= new LinkedList<Character>();
		
		String str = getInstance().getProperty("operators");
		StringTokenizer st = new StringTokenizer(str, ",");
		while (st.hasMoreTokens()) {
			opLinkedList.add(st.nextToken().charAt(0));	
		}
		
			return opLinkedList;
	}
	
	public static String getTrainigXString()
	{
		return getInstance().getProperty("trainingX");
	}
	
	public static String getTrainingYString()
	{
		return getInstance().getProperty("trainingY");
	}
	
	public static List<TrainingDatum> getTrainingData() {
		List<TrainingDatum> trainingData = new ArrayList<TrainingDatum>();
		
		String[] xValues = getInstance().getProperty("trainingX").split(",");
		String[] yValues = getInstance().getProperty("trainingY").split(",");
		
		for (int i=0; i<xValues.length; i++) {
			double x = Double.parseDouble(xValues[i]);
			double y = Double.parseDouble(yValues[i]);
			TrainingDatum datum = new TrainingDatum(x, y);
			trainingData.add(datum);
		}
		
		return trainingData;
	}
	
	//not sure, may be X must be special operand and function should return Doubles, for now
	public static String getOperandString()
	{
		return getInstance().getProperty("operands");
	}
	
	public static List<Character> getOperandList()
	{
		LinkedList<Character> opLinkedList= new LinkedList<Character>();
		
		String str = getInstance().getProperty("operands");
		StringTokenizer st = new StringTokenizer(str, ",");
		while (st.hasMoreTokens()) {
			opLinkedList.add(st.nextToken().charAt(0));	
		}
		
			return opLinkedList;	
	}
	
	public static List<Character> getIndependentOperandList()
	{
		LinkedList<Character> opLinkedList= new LinkedList<Character>();
		
		String str = getInstance().getProperty("independent");
		StringTokenizer st = new StringTokenizer(str, ",");
		while (st.hasMoreTokens()) {
			opLinkedList.add(st.nextToken().charAt(0));	
		}
		
			return opLinkedList;	
	}
	
	public static double getCrossoverProbability() {
		return getInstance().getDoubleProperty("crossoverProbability");
	}
	
	public static void setCrossoverProbability(Double size)
	{
		
		setDoubleProperty("crossoverProbability",size);
	}
	
	public static int getTournamentSize() {
		return getInstance().getIntProperty("tournamentSize");
	}
	
	public static void setTournamentSize(Integer size)
	{
		setIntProperty("tournamentSize",size);
	}
	
	public static int getMaxGenerations() {
		return getInstance().getIntProperty("maxGenerations");
	}
	
	public static void setMaxGenerations(Integer size)
	{
		setIntProperty("maxGenerations",size);
	}
	
	public static GPProperties getInstance() 
	{
		if (instance == null) {
			instance = new GPProperties();
		}
		return instance;
	}
	
	public boolean getBooleanProperty(String key) 
	{
		return Boolean.parseBoolean(getProperty(key));
	}
	
	public int getIntProperty(String key) 
	{
		return Integer.parseInt(getProperty(key));
	}
	
	public long getLongProperty(String key) {
		return Long.parseLong(getProperty(key));
	}
	
	public double getDoubleProperty(String key)
	{
		return Double.parseDouble(getProperty(key));
	}
	
	private static void setIntProperty(String key, Integer value)
	{
		try {
			try {
				 outputStream = new FileOutputStream(new File(propertiesFile));
			}
			catch (IOException e) {
				System.err.println("IOException: Unable to open outoutstream");
			}	
			
		getInstance().setProperty(key, "" + value);
		getInstance().store(outputStream,null);
		
		outputStream.close();
		}
		catch (IOException e)
		{
			System.err.println("IOException: Unable to store " + key);
		}
	}
	
	private static void setDoubleProperty(String key, Double value)
	{
		try {
			try {
				 outputStream = new FileOutputStream(new File(propertiesFile));
			}
			catch (IOException e) {
				System.err.println("IOException: Unable to open outoutstream");
			}	
			
		getInstance().setProperty(key, "" + value);
		getInstance().store(outputStream,null);
		
		outputStream.close();
		}
		catch (IOException e)
		{
			System.err.println("IOException: Unable to store " + key);
		}
	}
}