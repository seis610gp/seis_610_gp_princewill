package edu.stthomas.seis610.gp;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.stthomas.seis610.util.GPException;
import edu.stthomas.seis610.util.GPUtil;

import edu.stthomas.seis610.tree.BinaryNode;
import edu.stthomas.seis610.tree.BinaryTree;
import edu.stthomas.seis610.tree.OperandNode;
import edu.stthomas.seis610.tree.OperatorNode;
import edu.stthomas.seis610.tree.BinaryNode.NodeType;

/**
 * One genetic programming individual represented as a binary tree.
 *
 */
public class GPIndividual extends BinaryTree implements Comparable<GPIndividual>, Cloneable {
	private static Logger logger = LoggerFactory.getLogger(GPIndividual.class);
	Double fitness;
	List<TrainingDatum> trainingData;
	
	public GPIndividual(List<TrainingDatum> trainingData) {
		this.trainingData = trainingData;
	}
	
	public GPIndividual(BinaryNode root) {
		super(root);
	}
	
	public void mutate() throws GPException {
		// Get a random node
		BinaryNode node = getRandomNode();
		
		// Select a height for the new subtree
		int maxHeight = GPProperties.getMaxMutationDepth();
		int subtreeHeight = GPUtil.getInstance().getRandomInt(maxHeight)+1;
		
		// Generate subtree
		GPIndividual subtree = GPFactory.growIndividual(subtreeHeight);
		BinaryNode subtreeNode = subtree.getRoot();
		
		if (node.getNodeType() == NodeType.ROOT || node.getParent() == null) {
			// If the root node was selected, replace it with the new subtree
			this.setRoot(subtreeNode);
		} else {
			// Otherwise, replace the selected node with the new subtree
			subtreeNode.setParent(node.getParent());
			if (node.getNodeType() == NodeType.LEFT) {
				node.getParent().setLeft(subtreeNode);
				node.setNodeType(NodeType.LEFT);
			}
			else {
				node.getParent().setRight(subtreeNode);
				node.setNodeType(NodeType.RIGHT);
			}
		}
		
		// Reset fitness
		setFitness(null);
	}

	public double getFitness() throws GPException {
		logger.trace("getFitness() called for {}", toString());
		if (fitness == null) {
			fitness = new Double(0);
			List<BinaryNode> postOrderList = getPostOrderList();
			for (TrainingDatum trainingDatum : trainingData) {
				fitness = fitness + Math.abs(trainingDatum.getOutput() - evaluate(trainingDatum.getInput(), postOrderList));
			}
		}
		logger.trace("returning fitness {}", fitness);
		return fitness;
	}
	
	/**
	 * A non-recursive way to evaluate the function represented by a tree.
	 * 
	 * @param input The input value
	 * @param postOrderList The tree represented as a post order list
	 * @return The function output
	 * @throws GPException
	 */
	public double evaluate(double input, List<BinaryNode> postOrderList) throws GPException {
		LinkedList<Object> stack = new LinkedList<Object>();
		for (BinaryNode node : postOrderList) {
			if (node instanceof OperandNode) {
				stack.push(node);
			} else {
				OperatorNode operator = (OperatorNode) node;
				OperandNode right = (OperandNode) stack.pop();
				double rightValue = right.evaluate(input);
				OperandNode left = (OperandNode) stack.pop();
				double leftValue = left.evaluate(input);
				switch (operator.getOperator()) {
				case '+':
					stack.push(new OperandNode(leftValue + rightValue));
					break;
				case '-':
					stack.push(new OperandNode(leftValue - rightValue));
					break;
				case '*':
					stack.push(new OperandNode(leftValue * rightValue));
					break;
				case '/':
					stack.push(new OperandNode(leftValue / rightValue));
					break;
				default:
					throw new GPException("Invalid operator encountered during evaluation: " + operator.getOperator());
				}
			}
		}
		OperandNode resultNode = (OperandNode) stack.pop();
		return (Double) resultNode.evaluate(input);
	}
	
	/**
	 * A recursive way to evaluate the function represented by this tree.
	 * 
	 * @param input The input value
	 * @return The function output
	 * @throws GPException
	 */
	public double evaluateRecursive(double input) throws GPException {
		return getRoot().evaluate(input);
	}
	
	public OperatorNode getRandomOperatorNode() {
		List<BinaryNode> nodes = getPostOrderList();
		for (Iterator<BinaryNode> iter = nodes.iterator(); iter.hasNext();) {
			BinaryNode node = iter.next();
			if (node instanceof OperandNode)
				iter.remove();
		}
		int index = GPUtil.getInstance().getRandomInt(nodes.size());
		return (OperatorNode) nodes.get(index);
	}

	/**
	 * Returns one node from this individual at random.
	 * 
	 * @return A random node from this individual
	 */
	public BinaryNode getRandomNode() {
		List<BinaryNode> nodes = getPostOrderList();
		int index = GPUtil.getInstance().getRandomInt(nodes.size());
		return nodes.get(index);
	}
	
	public String toString() {
		return toString(getRoot());
	}
	
	private String toString(BinaryNode node) {
		StringBuffer output;
		if (node instanceof OperandNode) {
			return node.toString();
		} else {
			output = new StringBuffer();
			if (node != getRoot())
				output.append("(");
			output.append(this.toString(node.getLeft()));
			output.append(node.toString());
			output.append(this.toString(node.getRight()));
			if (node !=getRoot())
				output.append(")");
		}
		
		return output.toString();
	}

	@Override
	public int compareTo(GPIndividual o) {
		// TODO cleanup
		Double thisFitness = new Double(0);
		Double thatFitness = new Double(0);
		try {
			thisFitness = this.getFitness();
			thatFitness = o.getFitness();
		} catch (GPException e) {
			logger.error("GPIndividual.compareTo(): " + e.getMessage());
		}
		return thisFitness.compareTo(thatFitness);
	}

	public void setFitness(Double fitness) {
		this.fitness = fitness;
	}
	
	public Object clone() {
		GPIndividual clone = (GPIndividual) super.clone();
		clone.setTrainingData(getTrainingData());
		
		return clone;
	}

	public List<TrainingDatum> getTrainingData() {
		return trainingData;
	}

	public void setTrainingData(List<TrainingDatum> trainingData) {
		this.trainingData = trainingData;
	}
}
