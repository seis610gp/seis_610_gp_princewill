package edu.stthomas.seis610.gp;

import java.awt.*;
import java.awt.geom.*;

import java.util.*;

class GPColor
{
	public int iRed;
	public int iGreen;
	public int iBlue;
	
	public GPColor(int iRed, int iGreen, int iBlue)
	{
		this.iRed = iRed;
		this.iGreen = iGreen;
		this.iBlue = iBlue;
	}
}

abstract class GPGraphic
{
	protected static GPGraphicsView gpView;
	
	protected GPColor color;
	
	protected boolean bVisible;
	
	protected GPGraphic()
	{
		SetColor(0,0,0);
		bVisible = true;
	}
	
	public static void SetCurrentView(GPGraphicsView view)
	{
		gpView = view;
		gpView.gList.clear();
		gpView.rectBackground.Add();
	}
	
	public static void DrawViewElements()
	{
		Iterator<GPGraphic> itor = gpView.gList.iterator(); 
		
		while (itor.hasNext())
		{
			GPGraphic graphic = itor.next();
			if (graphic.bVisible)
			{
				Color c = new Color(graphic.color.iRed, graphic.color.iGreen, graphic.color.iBlue);
				gpView.g2d.setPaint(c);
				graphic.Draw();
			}
		}
		
		gpView.gList.clear();
	}
	
	public void Add()
	{
		gpView.gList.add(this);
	}
	public void Draw() {}
	public void SetColor(int iRed, int iGreen, int iBlue)
	{
		color = new GPColor(iRed,iGreen,iBlue);
	}
	
	public void SetColor(GPColor c) 
	{
		color = c;
	}
	
	public void SetVisible(boolean v)
	{
		bVisible = v;		
	}
}

class GPPoint extends GPGraphic
{
	private double dX;
	private double dY;
	
	public GPPoint(double x, double y)
	{
		dX = x;
		dY = y;		
	}
	
	void SetX(double x) {dX = x;}
	double GetX() {return dX;}
	void SetY(double y) {dY = y;}
	double GetY() {return dY;}
	
}

class GPLine extends GPGraphic
{
	
	private double dX1, dY1, dX2, dY2;
	private int 		iWidth;

	public GPLine(double dX1, double dY1, double dX2, double dY2)
	{
		this.dX1 = dX1;
		this.dY1 = dY1;
		this.dX2 = dX2;
		this.dY2 = dY2;
		this.iWidth = 1;
	}
	
	public void SetLineWidth(int width)
	{
		
		this.iWidth = width;
	}
	
	public void Draw()
	{
	//	gpView.dLeftX
		
		Line2D.Double line2d = new Line2D.Double(gpView.dLeftX + dX1,gpView.dBottomY - dY1,gpView.dLeftX + dX2,gpView.dBottomY - dY2);
		gpView.g2d.setStroke(new BasicStroke(iWidth));
		gpView.g2d.draw(line2d);
		gpView.g2d.setStroke(new BasicStroke(1));	
	}
}

class GPFillRectangle extends GPGraphic
{
	protected double dLeftX;
	protected double dTopY;
	protected double dRightX;
	protected double dBottomY;
	public GPFillRectangle(double dLeftX, double dTopY, double dRightX, double dBottomY)
	{
		this.dLeftX = dLeftX;
		this.dTopY = dTopY;
		this.dRightX = dRightX;
		this.dBottomY = dBottomY;
	}
	
	public double width() {	return dRightX - dLeftX;}
	
	public double height() {return dBottomY - dTopY;}	
	
	public void Draw()
	{
		Rectangle2D.Double rect2d = new Rectangle2D.Double(dLeftX,dTopY,width(),height());
		gpView.g2d.fill(rect2d);
	}
}

class GPPolygon extends GPGraphic
{
	private Vector<GPPoint> polygon;
	private int iPolygonWidth;
	
	public GPPolygon(Vector<GPPoint> v)
	{
		polygon = v;
		iPolygonWidth = 1;
	}
	
	void SetPolygonWidth(int width)
	{
		iPolygonWidth = width;	
	}
	
	public void Draw()
	{
		if (polygon.size() > 1)
		{
			Iterator<GPPoint> iter = polygon.iterator();
			
			GPPoint point1 = iter.next();
			while (iter.hasNext())
			{
				GPPoint point2 = iter.next();
		
				Line2D.Double line2d = new Line2D.Double(gpView.dLeftX + point1.GetX(),gpView.dBottomY - point1.GetY(),gpView.dLeftX + point2.GetX(),gpView.dBottomY - point2.GetY());
				gpView.g2d.setStroke(new BasicStroke(iPolygonWidth));
				gpView.g2d.draw(line2d);
				gpView.g2d.setStroke(new BasicStroke(1));	
				
				point1 = point2;
			}
				
		}
		
	}

}

class GPString extends GPGraphic
{
	private String cString;
	private double dLeftX;
	private double dBottomY;
	
	public GPString(String str, double dX, double dY)
	{
		this.cString 	= str;
		this.dLeftX		= dX;
		this.dBottomY 	= dY;
	}
	
	public void SetText(String newText)
	{
		this.cString	= newText;		
	}
	
	public void Draw()
	{
		gpView.g2d.drawString(cString,(int)(gpView.dLeftX + dLeftX),(int)(gpView.dBottomY - dBottomY));
	}
	
	public void SetPosition(double x, double y)
	{
		dLeftX = x;
		dBottomY = y;
	}
	
}

public class GPGraphicsView extends GPGraphic
{
	public double dLeftX;
	public double dTopY;
	public double dRightX;
	public double dBottomY;
	protected GPFillRectangle rectBackground;
	public Graphics2D g2d;
	private String 	viewName;
	
	public GPGraphicsView(double dLeftX, double dTopY, double dWidth, double dHeight, String name)
	{
		gList = new LinkedList<GPGraphic>();
		
		this.dLeftX 	= dLeftX;
		this.dTopY 		= dTopY;
		this.dRightX 	= dLeftX + dWidth;
		this.dBottomY	= dTopY + dHeight;
		
		rectBackground = new GPFillRectangle(this.dLeftX,this.dTopY,this.dRightX,this.dBottomY);
		rectBackground.SetColor(0,0,0);
		
		viewName = name;
	}
	
	public void SetGraphicsContent(Graphics2D g2d)
	{
		this.g2d = g2d;
		
		g2d.setClip((int)dLeftX,(int)dTopY,(int)width(),(int)height());
	}
	
	public double width() {return rectBackground.width();}
	
	public double height() {return rectBackground.height();}
	
	public boolean isInside(double x, double y)
	{
		return (x < width() && x > 0 && y < height() && y > 0);
	}
	
	
	
	public void move(double dLeftX, double dTopY)
	{
		this.dLeftX		= dLeftX;
		this.dTopY 		= dTopY;
		dRightX 		= dLeftX + rectBackground.width();
		dBottomY 		= dTopY + rectBackground.height(); 
	}
	
	public void SetBackgroundColor(int iRed,int iGreen, int iBlue)
	{
		rectBackground.SetColor(new GPColor(iRed,iGreen,iBlue));
	}
	
	protected LinkedList<GPGraphic> gList;
}