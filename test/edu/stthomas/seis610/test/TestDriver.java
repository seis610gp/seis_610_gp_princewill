package edu.stthomas.seis610.test;

import org.junit.runner.JUnitCore;

public class TestDriver {
	public static void main(String[] args) {
		JUnitCore.runClasses(GPTest.class);
	}
}
