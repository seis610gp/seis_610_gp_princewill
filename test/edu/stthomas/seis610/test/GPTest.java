package edu.stthomas.seis610.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.stthomas.seis610.tree.BinaryNode;
import edu.stthomas.seis610.gp.GPFactory;
import edu.stthomas.seis610.gp.GPIndividual;
import edu.stthomas.seis610.gp.GPProperties;
import edu.stthomas.seis610.gp.TrainingDatum;
import edu.stthomas.seis610.tree.OperandNode;
import edu.stthomas.seis610.tree.OperatorNode;
import edu.stthomas.seis610.util.GPException;

public class GPTest {
	OperatorNode divide;
	OperatorNode plus;
	OperandNode six;
	OperandNode two;
	OperandNode four;
	GPIndividual individual;
	GPIndividual perfectIndividual;
	
	@Before
	public void initialize() throws GPException {
		divide = new OperatorNode('/');
		plus = new OperatorNode('+');
		six = new OperandNode('6');
		two = new OperandNode('2');
		four = new OperandNode('4');
		
		plus.setLeft(six);
		plus.setRight(two);
		two.setParent(plus);
		six.setParent(plus);
		divide.setLeft(plus);
		divide.setRight(four);
		plus.setParent(divide);
		four.setParent(divide);
		
		individual = new GPIndividual(divide);
		
		OperatorNode root = new OperatorNode('/');
		root.setRight(new OperandNode('2'));
		OperatorNode second = new OperatorNode('+');
		root.setLeft(second);
		second.setRight(new OperandNode('2'));
		OperatorNode third = new OperatorNode('*');
		second.setLeft(third);
		third.setRight(new OperandNode('x'));
		third.setLeft(new OperandNode('x'));
		perfectIndividual = new GPIndividual(root);
		List<TrainingDatum> trainingData = GPProperties.getTrainingData();
		perfectIndividual.setTrainingData(trainingData);
	}
	
	@Test
	public void maxDepth() throws GPException {
		assertEquals(individual.maxDepth(), 3);
	}
	
	@Test
	public void depth() {
		assertEquals(plus.getDepth(), 2);
	}
	
	@Test
	public void postOrderList() throws GPException {
		List<BinaryNode> postOrderList = individual.getPostOrderList();
		List<BinaryNode> referenceList = new ArrayList<BinaryNode>(5);
		referenceList.add(six);
		referenceList.add(two);
		referenceList.add(plus);
		referenceList.add(four);
		referenceList.add(divide);
		 
	    assertEquals(postOrderList, referenceList);
	}
	
	@Test
	public void evaluate() throws GPException {
		double result = individual.evaluate(4.0, individual.getPostOrderList());
		assertEquals(result, 2.0, 0);
	}
	
	@Test
	public void evaluateRecursive() throws GPException {
		double result = individual.evaluateRecursive(4.0);
		assertEquals(result, 2.0, 0);
	}
	
	@Test
	public void perfectFitness() throws GPException {
		double result = perfectIndividual.getFitness();
		assertEquals(result, 0.0, 0);
	}
	
	@Test
	public void readProperty() {
		int populationSize = GPProperties.getPopulationSize();
		assertEquals(populationSize, 100);
	}
	
	@Test
	public void printTree() {
		String output = "(6+2)/4";
		assertEquals(individual.toString(), output);
	}
	
	@Test
	public void deepCopy() {
		GPIndividual copy = (GPIndividual) individual.clone();
		assertNotSame(individual, copy);
		assertNotSame(individual.getRoot(), copy.getRoot());
		assertNotSame(individual.getRoot().getLeft(), copy.getRoot().getLeft());
		
		assertEquals(individual.getRoot().getData(), copy.getRoot().getData());
		// TODO deep copy is working for everything except the actual data of the node
		// does this matter?
		assertNotSame(individual.getRoot().getData(), copy.getRoot().getData());
	}
	
	@Test
	public void fullInividual() throws GPException {
		int maxDepth = GPProperties.getMaxTreeDepth();
		GPIndividual fullIndividual = GPFactory.fullIndividual(maxDepth);
		assertEquals(fullIndividual.maxDepth(), maxDepth);
	}
	
	@Test
	public void growIndividual() throws GPException {
		int maxDepth = GPProperties.getMaxTreeDepth();
		GPIndividual growIndividual = GPFactory.growIndividual(maxDepth);
		assertTrue(growIndividual.maxDepth() <= maxDepth);
	}
}
